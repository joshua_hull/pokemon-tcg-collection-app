#!/bin/bash
BUCKET="$1"

if [ "$(aws s3api list-buckets | tr -d '"' | grep $BUCKET | wc -l)" -lt "1" ]; then
    aws --region us-east-1 s3 mb s3://$BUCKET
fi
aws --region us-east-1 s3api put-bucket-acl --acl public-read --bucket $BUCKET
aws --region us-east-1 s3 website s3://$BUCKET/ --index-document index.html --error-document index.html
aws --region us-east-1 s3 sync --acl public-read build s3://$BUCKET