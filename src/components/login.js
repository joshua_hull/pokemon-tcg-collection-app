import React, {Component} from 'react';
import {Row, Col, FormGroup, FormControl, ControlLabel, Button, Panel, Alert} from 'react-bootstrap';
import {connect} from 'react-redux';
import {setLoginError, setLogin} from '../redux/actions';
import './login.css';

import {APIService} from '../services/RESTService';
const MyAPIService = new APIService({endpoint: process.env.REACT_APP_API_HOSTNAME});

const mapStateToProps = state => {
    return {
        loginError: state.login.loginError,
        jwt: state.login.jwt
    }
}

const mapDispatchToProps = dispatch => {
    return {
        loginErrorHandler: error => {
            dispatch(setLoginError(error));
        },
        loginHandler: jwt => {
            dispatch(setLogin(jwt));
        }
    }
}

class login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            modalDismissed: false
        };

        this.handleLogin = this.handleLogin.bind(this);
        this.handleUsername = this.handleUsername.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.handleModalDismissed = this.handleModalDismissed.bind(this);
    }

    handleLogin(e) {
        e.preventDefault();
        this.setState({modalDismissed: false}, ()=> {
            MyAPIService.login(this.state.username, this.state.password, this.props.loginHandler, this.props.loginErrorHandler);
        });
    }

    handleUsername(e) {
        this.setState({ username: e.target.value});
    }

    handlePassword(e) {
        this.setState({password: e.target.value});
    }

    handleModalDismissed(e) {
        this.setState({modalDismissed: true});
    }

    render() {
        let alert = '';
        if (this.props.loginError && !this.state.modalDismissed) {
            alert = <Alert onDismiss={this.handleModalDismissed} bsStyle="danger">
            <Panel>
                <Panel.Heading>
                    <Panel.Title>
                        Login Error
                    </Panel.Title>
                    <Panel.Toggle componentClass="a">There was an error logging in. Please try again later.</Panel.Toggle>
                </Panel.Heading>
                <Panel.Collapse>
                    <Panel.Body>
                        <h3><code>{this.props.loginError.message}</code></h3>
                        {this.props.loginError.stack.split('\n').map((item, i) => { return <p key={i}><code>{item}</code></p>})}
                    </Panel.Body>
                </Panel.Collapse>
            </Panel>
        </Alert>
        };
        return (
        <div className="container">
            {alert}            
            <Row>
                <Col xs={10} xsOffset={1} md={6} mdOffset={3} id='login-box'>
                    <form action="#">
                        <FormGroup controlId='username'>
                            <ControlLabel>Username</ControlLabel>
                            <FormControl type="text" value={this.state.username} onChange={this.handleUsername}/>
                        </FormGroup>
                        <FormGroup controlId='password'>
                            <ControlLabel >Password</ControlLabel>
                            <FormControl type="password" value={this.state.password} onChange={this.handlePassword}/>
                        </FormGroup>
                        <Button type="submit" onClick={this.handleLogin}>Login</Button>
                    </form>
                </Col>
            </Row>
        </div>

        );
    }
}

const LoginContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(login)

export default LoginContainer;