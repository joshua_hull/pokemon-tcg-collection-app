const axios = require('axios');
const qs = require('qs');

class APIService {
    constructor(params) {
        this.endpoint = params.endpoint;
    }

    login(username, password, successHandler, errorHandler) {
        axios.post(`${this.endpoint}/auth/login`, qs.stringify({username, password}), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}})
        .then((resp) => {
            successHandler(resp.data.jwt);
        })
        .catch((error) => {
            errorHandler(error);
        })
    }
}

export {APIService};