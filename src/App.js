import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import {connect} from 'react-redux';
import app from "./components/app";
import login from './components/login';

const mapStateToProps = state => {
  return {
      jwt: state.login.jwt
  }
}

const mapDispatchToProps = dispatch => {
  return {}
}

class App extends Component {
  render() {
    let defaultComponent = app;
    if(!this.props.jwt) {
      defaultComponent = login;
    }
    return (
      <Router>
        <div>
          <Route path='/' component={defaultComponent}/>
        </div>
      </Router>
    );
  }
}

const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(App)

export default AppContainer;