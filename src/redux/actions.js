export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGIN = 'LOGIN';

export function setLoginError(error) {
    return {type: LOGIN_ERROR, error};
}

export function setLogin(jwt) {
    return {type: LOGIN, jwt};
}