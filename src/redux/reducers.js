import {combineReducers} from 'redux';
import {
    LOGIN_ERROR,
    LOGIN
} from './actions';

function login(state = {}, action) {
    switch (action.type) {
        case LOGIN_ERROR:
            return Object.assign({}, state, {
                loginError: action.error
            });
        case LOGIN:
            return Object.assign({}, state, {
                jwt: action.jwt,
                loginError: null
            });
        default:
            return state
    }
}

const app = combineReducers({
    login
})

export default app;