import React from 'react';
import ReactDOM from 'react-dom';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import app from './redux/reducers';
import App from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const store = createStore(app);
  ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
